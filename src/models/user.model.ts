import Sequelize from 'sequelize';
import {
  BelongsTo,
  Column,
  HasMany,
  HasOne,
  Model,
  Table,
} from 'sequelize-typescript';

import { UserStatus } from '../common/eNum/userStatus.eNum';
import { SmsRq } from './smsRq.model';

@Table({
  tableName: 'users',
  paranoid: true,
  deletedAt: 'deletedAt',
})
export class User extends Model {
  @Column({
    primaryKey: true,
    autoIncrement: true,
    allowNull: false,
    type: Sequelize.INTEGER,
  })
  id: number;

  @Column({
    allowNull: true,
    type: Sequelize.STRING,
  })
  firstName: string;

  @Column({
    allowNull: true,
    type: Sequelize.STRING,
  })
  lastName: string;

  @Column({
    allowNull: true,
    type: Sequelize.STRING,
  })
  username: string;

  @Column({
    allowNull: true,
    type: Sequelize.STRING,
  })
  token: string;

  @Column({
    type: Sequelize.STRING,
    allowNull: true,
  })
  password: string;

  @Column({
    type: Sequelize.STRING,
    allowNull: true,
  })
  phone: string;

  @Column({
    type: Sequelize.STRING,
    allowNull: true,
  })
  lat: string;

  @Column({
    type: Sequelize.STRING,
    allowNull: true,
  })
  lon: string;

  @Column({
    type: Sequelize.STRING,
    allowNull: true,
  })
  avatar: string;

  @Column({
    allowNull: false,
    type: Sequelize.ENUM({ values: Object.keys(UserStatus) }),
    defaultValue: UserStatus.offline,
  })
  userStatus: UserStatus;

  @HasOne(() => SmsRq, { foreignKey: 'userId' })
  SmsRq: SmsRq;
}
