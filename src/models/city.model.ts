import Sequelize from 'sequelize';
import { BelongsTo, Column, HasMany, Model, Table } from 'sequelize-typescript';

import { Province } from './province.model';
import { User } from './user.model';

@Table({
  tableName: 'cities',
  paranoid: true,
  deletedAt: 'deletedAt',
})
export class City extends Model {
  @Column({
    primaryKey: true,
    autoIncrement: true,
    allowNull: false,
    type: Sequelize.INTEGER,
  })
  id: number;

  @Column({
    type: Sequelize.INTEGER,
    allowNull: false,
    references: { model: 'provinces', key: 'id' },
  })
  provinceId: number;

  @Column({
    type: Sequelize.STRING,
    allowNull: false,
  })
  name: string;

  @Column({
    type: Sequelize.FLOAT,
    allowNull: true,
  })
  lat: number;

  @Column({
    allowNull: true,
    type: Sequelize.FLOAT,
  })
  lng: number;

  @Column({
    defaultValue: new Date(),
    allowNull: false,
    type: Sequelize.DATE,
  })
  createdAt: Date;

  @Column({
    defaultValue: new Date(),
    allowNull: false,
    type: Sequelize.DATE,
  })
  updatedAt: Date;

  @Column({
    allowNull: true,
    type: Sequelize.DATE,
  })
  deletedAt: Date;

  @BelongsTo(() => Province, { foreignKey: 'provinceId' })
  Province: Province;

  @HasMany(() => User, { foreignKey: 'cityId' })
  Users: User[];
}
