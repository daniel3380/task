import {
  Column,
  Table,
  Model,
  BelongsTo,
  HasMany,
  HasOne,
} from 'sequelize-typescript';
import Sequelize from 'sequelize';
import { User } from './user.model';

@Table({
  tableName: 'smsRq',
  paranoid: true,
  deletedAt: 'deletedAt',
})
export class SmsRq extends Model {
  @Column({
    primaryKey: true,
    autoIncrement: true,
    allowNull: false,
    type: Sequelize.INTEGER,
  })
  id: number;

  @Column({
    allowNull: false,
    type: Sequelize.STRING,
  })
  mobile: string;

  @Column({
    allowNull: true,
    type: Sequelize.INTEGER,
  })
  code: number;

  @Column({
    allowNull: false,
    type: Sequelize.STRING,
  })
  ip: string;

  @Column({
    allowNull: false,
    type: Sequelize.DATE,
  })
  lastSentTime: Date;

  @Column({
    allowNull: true,
    type: Sequelize.INTEGER,
  })
  sendCount: number;

  @Column({
    allowNull: false,
    type: Sequelize.STRING,
  })
  jwtToken: string;

  @HasOne(() => User, { foreignKey: 'id' })
  User: User;
}
