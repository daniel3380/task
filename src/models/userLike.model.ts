import Sequelize from 'sequelize';
import { BelongsTo, Column, Model, Table } from 'sequelize-typescript';
import { LikeType } from '../common/eNum/likeType.eNum';
import { User } from './user.model';

@Table({
  tableName: 'user_likes',
  paranoid: false,
  deletedAt: 'deletedAt',
})
export class UserLike extends Model {
  @Column({
    primaryKey: true,
    autoIncrement: true,
    allowNull: false,
    type: Sequelize.INTEGER,
  })
  id: number;

  @Column({
    allowNull: true,
    type: Sequelize.INTEGER,
    references: { model: 'users', key: 'id' },
  })
  userSenderId: number;

  @Column({
    allowNull: true,
    type: Sequelize.INTEGER,
    references: { model: 'users', key: 'id' },
  })
  userGetterId: number;

  @Column({
    allowNull: false,
    type: Sequelize.ENUM({ values: Object.keys(LikeType) }),
  })
  likeType: LikeType;

  @BelongsTo(() => User, { foreignKey: 'userSenderId' })
  UserSenderId: User;

  @BelongsTo(() => User, { foreignKey: 'userGetterId' })
  UserGetterId: User;
}
