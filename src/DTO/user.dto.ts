import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsNumberString, IsOptional } from 'class-validator';

export function userMidObj(user, token = null) {
  return {
    id: user.id,
    lastName: user.lastName,
    firstName: user.firstName,
    username: user.username,
    phone: user.phone,
    avatar: user.avatar,
    token,
  };
}

export function userObj(user) {
  return {
    id: user.id,
    lastName: user.lastName,
    firstName: user.firstName,
    username: user.username,
    phone: user.phone,
    avatar: user.avatar,
    token: user.token,
  };
}

export function userSmall(user) {
  return {
    id: user.id,
    lastName: user.lastName,
    firstName: user.firstName,
  };
}

export class UserDto {
  @IsNumberString()
  phone: string;

  @ApiProperty({ type: String })
  lastName: string;

  @ApiProperty({ type: String })
  firstName: string;

  @ApiProperty({ type: String })
  username: string;

  @ApiProperty({ type: String })
  token: string;

  @ApiProperty({ type: String })
  avatar: string;
}

export class LoginStepTwoDto {
  @IsNumberString()
  @ApiProperty({ type: String })
  @IsNotEmpty({ message: 'شماره موبایل اجباری میباشد' })
  phone: string;

  @IsNotEmpty({ message: ' مشکلی پیش آمده است ' })
  @ApiProperty({ type: String })
  token: string;

  @IsNotEmpty({ message: 'کد تایید اجباری میباشد' })
  @ApiProperty({ type: Number })
  code: number;
}

export class LoginDto {
  @IsNumberString()
  @ApiProperty({ type: String })
  @IsNotEmpty({ message: 'شماره موبایل اجباری میباشد' })
  phone: string;

  @ApiProperty({ type: String })
  @IsNotEmpty({ message: 'نوع تماس اجباری میباشد' })
  type: string;
}

export class LoginDtoResponse {
  @ApiProperty({ type: String })
  type: string;

  @ApiProperty({ type: Boolean })
  status: boolean;
}

export class TokenDto {
  @IsNumberString()
  @ApiProperty({ type: String })
  @IsNotEmpty({ message: 'شماره موبایل اجباری میباشد' })
  phone: string;

  @IsNumberString()
  @IsNotEmpty({ message: ' مشکلی پیش آمده است ' })
  @ApiProperty({ type: String })
  token: string;

  @IsNumberString()
  @IsNotEmpty({ message: 'کد تایید اجباری میباشد' })
  @ApiProperty({ type: String })
  code: string;
}

export class LoginBodyDto {
  @IsNumberString()
  @ApiProperty({ type: String })
  @IsNotEmpty({ message: 'شماره موبایل اجباری میباشد' })
  phone: string;

  @IsNumberString()
  @ApiProperty({ type: String })
  @IsNotEmpty({ message: 'نوع تماس اجباری میباشد' })
  type: string;
}

export class CreateUserDto {
  @ApiProperty({ type: String })
  @IsOptional()
  firstName: string;

  @ApiProperty({ type: String })
  @IsOptional()
  lastName: string;

  @IsNumberString()
  @ApiProperty({ type: String })
  @IsNotEmpty({ message: 'شماره موبایل اجباری می باشد' })
  phone: string;
}

export class AddUsernameDto {
  @ApiProperty({ type: String })
  @IsNotEmpty({ message: 'نام کاربری اجباری می باشد' })
  username: string;
}

export class AddAvatarDto {
  @ApiProperty({ type: String })
  @IsNotEmpty({ message: 'آواتار اجباری می باشد' })
  avatar: string;
}

export class UserDetailsDto {
  @ApiProperty({ type: String })
  @IsNotEmpty({ message: 'آیدی کاربری اجباری می باشد' })
  id: string;
}

export class NameFamilyDto {
  @ApiProperty({ type: String })
  @IsNotEmpty({ message: ' نام اجباری می باشد ' })
  firstName: string;

  @ApiProperty({ type: String })
  @IsNotEmpty({ message: ' نام خانوادگی اجباری می باشد' })
  lastName: string;
}

export class UserSmallDto {
  @ApiProperty({ type: Number })
  id: number;

  @ApiProperty({ type: String })
  firstName: string;

  @ApiProperty({ type: String })
  lastName: string;
}
