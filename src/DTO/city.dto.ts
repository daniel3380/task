import { ApiProperty } from '@nestjs/swagger';
import { IsInt, IsNotEmpty } from 'class-validator';

// OBJ *******************

export function cityObj(city) {
  return {
    id: city.id,
    name: city.name,
    provinceId: city.provinceId,
    lat: city.lat,
    lng: city.lng,
  };
}
export function cityNameObj(city) {
  return city.name;
}

// CLASS *******************

export class CityNameDto {
  @IsNotEmpty()
  @ApiProperty({ type: Number })
  id: number;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  name: string;

  @ApiProperty({ type: Number })
  lat: number;

  @ApiProperty({ type: Number })
  lng: number;
}
export class CityDto {
  @IsNotEmpty()
  @ApiProperty({ type: Number })
  id: number;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  name: string;

  @IsNotEmpty()
  @ApiProperty({ type: Number })
  provinceId: number;

  @ApiProperty({ type: Number })
  lat: number;

  @ApiProperty({ type: Number })
  lng: number;
}
export class CreatecityDto {
  @IsNotEmpty()
  @ApiProperty({ type: String })
  name: string;

  @IsNotEmpty()
  @ApiProperty({ type: Number })
  provinceId: number;

  @IsNotEmpty()
  @ApiProperty({ type: Number })
  lat: number;

  @IsNotEmpty()
  @ApiProperty({ type: Number })
  lng: number;
}
export class UpdatecityDto {
  @IsNotEmpty()
  @IsInt()
  @ApiProperty({ type: Number })
  id: number;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  name: string;

  @IsNotEmpty()
  @ApiProperty({ type: Number })
  provinceId: number;
}
export class UpdateCtiyLocationDto {
  @IsNotEmpty()
  @IsInt()
  @ApiProperty({ type: Number })
  id: number;

  @IsNotEmpty()
  @ApiProperty({ type: Number })
  lat: number;

  @IsNotEmpty()
  @ApiProperty({ type: Number })
  lng: number;
}
