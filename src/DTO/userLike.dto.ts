import { ApiProperty } from '@nestjs/swagger';
import { IsEnum, IsNotEmpty } from 'class-validator';
import { LikeType } from '../common/eNum/likeType.eNum';

export function carLikeObj(car) {
  return {
    id: car.id,
    carId: car.carId,
    userId: car.userId,
  };
}

export class UserLikeDto {
  @ApiProperty({ type: Number })
  id: number;

  @ApiProperty({ type: Number })
  userSenderId: number;

  @ApiProperty({ type: Number })
  userGetterId: number;
}

export class CreateUserLikeDto {
  @ApiProperty({ type: Number })
  @IsNotEmpty({ message: '' })
  userGetterId: number;

  @IsEnum(LikeType)
  @ApiProperty({ type: String })
  likeType: string;
}

export class ResponseCarLikeDto {
  @ApiProperty({ type: Number })
  @IsNotEmpty({ message: '' })
  userGetterId: number;

  @ApiProperty({ type: String })
  @IsNotEmpty({ message: '' })
  message: string;
}
