import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { AppModule } from './app.module';

import * as swaggerConfig from './config/swagger.json';

import { Logger, ValidationPipe } from '@nestjs/common';
import { NestExpressApplication } from '@nestjs/platform-express';
import { GlobalService } from './utils/global.service';

async function bootstrap() {
  const app = await NestFactory.create<NestExpressApplication>(AppModule, {
    cors: true,
  });
  // app.setGlobalPrefix('api/v1');
  // app.useGlobalInterceptors(new ClassSerializerInterceptor(app.get(Reflector)));

  app.useGlobalPipes(new ValidationPipe());
  const logger = new Logger('APP');

  app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('X-Powered-By', 'NestJs');
    res.setHeader(
      'Access-Control-Allow-Methods',
      'GET, POST, OPTIONS, PUT, PATCH, DELETE',
    );
    res.setHeader(
      'Access-Control-Allow-Headers',
      'X-Requested-With,content-type',
    );
    res.setHeader('Access-Control-Allow-Credentials', true);
    res.header(
      'Access-Control-Allow-Headers',
      'Origin, X-Requested-With, Content-Type, Accept',
    );
    next();
  });

  // Get seeding services

  // Seeding roles
  // try {
  //   logger.verbose('roles seeding');
  //   await roleService.seed();
  //   logger.verbose('roles seeding done');
  // } catch (error) {
  //   logger.error('roles seeding failed.');
  //   console.log(error);
  // }

  // Seeding users
  // try {
  //   // wait 1s to make sure all the roles are seeded perfectly
  //   setTimeout(async () => {
  //     logger.verbose('users seeding');
  //     await userService.seed();
  //     logger.verbose('users seeding done');
  //   }, 1000);
  // } catch (error) {
  //   logger.error('users seeding failed.');
  //   console.log(error);
  // }

  // Seeding brands
  // try {
  //   // wait 1s to make sure all the roles are seeded perfectly
  //   setTimeout(async () => {
  //     logger.verbose('brands seeding');
  //     await brandService.seed();
  //     logger.verbose('brands seeding done');
  //   }, 1000);
  // } catch (error) {
  //   logger.error('brands seeding failed.');
  //   console.log(error);
  // }

  GlobalService.onlineUsers = new Map();
  const config = new DocumentBuilder()
    .setTitle(swaggerConfig.title)
    .setDescription(swaggerConfig.description)
    .setVersion(swaggerConfig.version)
    .addTag(swaggerConfig.tag)
    .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('api/v1', app, document);
  await app.listen(3000);
}
bootstrap();
