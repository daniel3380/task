import { Injectable, NestMiddleware } from '@nestjs/common';

@Injectable()
export class UsernameMiddleware implements NestMiddleware {
  use(req: any, res: any, next: any) {
    // Check if user exists on request
    if (!req.user) {
      return res.status(401).json({ message: 'User not found' });
    }
    next();
  }
}
