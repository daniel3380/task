import {
  UpdateDateColumn,
  CreateDateColumn,
  BaseEntity,
  DeleteDateColumn,
  Column,
} from 'typeorm';
import { Injectable } from '@nestjs/common';

@Injectable()
export abstract class AuditableEntity extends BaseEntity {
  @CreateDateColumn({ name: 'created_at' })
  createdAt: Date;

  @UpdateDateColumn({ name: 'updated_at' })
  updatedAt: Date;

  @DeleteDateColumn({ name: 'deleted_at' })
  deletedAt?: Date;
}
