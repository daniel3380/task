const userStatus = {
  online: 'online',
  offline: 'offline',
};
export default userStatus;
export enum UserStatus {
  online = 'online',
  offline = 'offline',
}
