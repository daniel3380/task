import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { ThrottlerModule } from '@nestjs/throttler';
import { AdminModule } from './application/admin/admin.module';
import { CommonModule } from './application/common/common.module';
import { UsersModule } from './application/user/user.module';
import { Jwt } from './common/helpers/jwt.helper';
import { UsernameMiddleware } from './common/middlewares/username.middleware';
import { CheckUserMiddleware } from './common/middlewares/validateUser.middleware';
import { SmsRqDataAcceess } from './dataAccess/smsRq.dataAccess';
import { UsersDataAcceess } from './dataAccess/users.dataAccess';
import sequilzeObj from './database/sequilze.obj';

@Module({
  imports: [
    // ServeStaticModule.forRoot({
    //   rootPat h: join(__dirname, '..', 'public'),
    // }),
    // ConfigModule.forRoot({
    //   isGlobal: true,
    // }),
    sequilzeObj,

    ThrottlerModule.forRoot({
      ttl: 60,
      limit: 10,
    }),
    AdminModule,
    CommonModule,
    UsersModule,
  ],
  controllers: [],
  providers: [UsernameMiddleware, SmsRqDataAcceess, UsersDataAcceess, Jwt],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(CheckUserMiddleware).forRoutes('*');
  }
}
