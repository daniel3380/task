import * as Models from '../models/index';

export class UserLikeDataAccess {
  async findOneId(userGetterId, likeType, userSenderId) {
    const result = await Models.UserLike.findOne({
      where: {
        userGetterId,
        likeType,
        userSenderId,
      },
    });
    return result;
  }

  async create(userGetterId, likeType, userSenderId) {
    await Models.UserLike.create({
      userSenderId,
      likeType,
      userGetterId,
    });
    return;
  }

  async destroy(userGetterId, likeType, userSenderId) {
    await Models.UserLike.destroy({
      where: { userSenderId, likeType, userGetterId },
    });
    return;
  }
}
