import { Client } from '@elastic/elasticsearch';
const { ELASTIC_USERNAME, ELASTIC_PASSWORD, ELASTIC_HOST } = process.env;
// Create a new Elasticsearch client instance
const client = new Client({
  node: ELASTIC_HOST, // Specify the Elasticsearch node URL
  auth: {
    username: ELASTIC_USERNAME, // Specify your Elasticsearch username
    password: ELASTIC_PASSWORD, // Specify your Elasticsearch password
  },
  // Additional configuration options can be added here
});

// // Example: Perform a simple query
// async function searchExample() {
//   try {
//     const { body } = await client.search({
//       index: 'your_index', // Specify the index you want to search
//       body: {
//         query: {
//           match: {
//             field_name: 'search_query', // Specify the field and query terms
//           },
//         },
//       },
//     });

//     console.log(body.hits.hits); // Output the search results
//   } catch (error) {
//     console.error(error);
//   }
// }

// // Example: Index a document
// async function indexExample() {
//   try {
//     const { body } = await client.index({
//       index: 'your_index', // Specify the index where you want to store the document
//       body: {
//         field_name: 'field_value', // Specify the field and its value
//       },
//     });

//     console.log(body); // Output the indexing response
//   } catch (error) {
//     console.error(error);
//   }
// }

// Call your example functions here
// searchExample();
// indexExample();
