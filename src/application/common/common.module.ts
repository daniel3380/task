import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';

import { Jwt } from '../../common/helpers/jwt.helper';
import { Tools } from '../../common/helpers/tools.helpers';
import { ValidUserMiddleware } from '../../common/middlewares/validateUser.middleware';
import { MediaDataAcceess } from '../../dataAccess/media.dataAccess';
import { StateDataAccess } from '../../dataAccess/state.dataAccess';
import { UsersDataAcceess } from '../../dataAccess/users.dataAccess';
import { CommonController } from './common.controller';
import { CommonService } from './common.service';

@Module({
  imports: [],
  controllers: [CommonController],
  providers: [
    CommonService,
    Tools,
    MediaDataAcceess,
    StateDataAccess,
    UsersDataAcceess,
    Jwt,
  ],
})
export class CommonModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(ValidUserMiddleware)
      .exclude('/user/login', '/user/verify')
      .forRoutes('/');
  }
}
