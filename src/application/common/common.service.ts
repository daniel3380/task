import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import * as shell from 'shelljs';

import { Tools } from '../../common/helpers/tools.helpers';
import { MediaDataAcceess } from '../../dataAccess/media.dataAccess';
import { StateDataAccess } from '../../dataAccess/state.dataAccess';
import { UsersDataAcceess } from '../../dataAccess/users.dataAccess';
// import { Cloud } from '../../upload/getUploadsList.service';

import { StateDto, stateObj } from '../../DTO/state.dto';

import * as fs from 'fs';
import pathes from '../../config/pathes';

@Injectable()
export class CommonService {
  constructor(
    private readonly mediaDataAcceess: MediaDataAcceess,
    private readonly stateDataAccess: StateDataAccess,
    private readonly tools: Tools,
    // private readonly cloud: Cloud,
    private readonly usersDataAcceess: UsersDataAcceess,
  ) {}
  // uploadfile ***************************************************
  async upload(addAvatarDto, userId) {
    const { buffer } = addAvatarDto;

    const user = await this.usersDataAcceess.findOneId(userId);
    if (!user) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          error: ' کاربری وجود ندارد ',
        },
        HttpStatus.FORBIDDEN,
      );
    }
    // const srcImage = await this.cloud.getUploadsList(buffer, 'avatar');
    // const file = await this.mediaDataAcceess.createMediaCloud(srcImage, userId);

    // // const resAvatar = await this.usersDataAcceess.avatar(srcImage, userId);
    // return file;
  }
  async uploadWithCompress(filename, dist) {
    !fs.existsSync(`${pathes.staticFiles}${pathes.uploadedFiles}${dist}`)
      ? fs.mkdir(
          `${pathes.staticFiles}${pathes.uploadedFiles}${dist}`,
          (err) => {
            console.log(err);
          },
        )
      : null;
    const mimeType = await this.tools.checkMime(
      `${pathes.staticFiles}${pathes.uploadedFiles}${filename}`,
    );
    if (mimeType === 1) {
      await shell.exec(
        'sudo  ffmpeg -i   ' +
          `${pathes.staticFiles}${pathes.uploadedFiles}${filename}` +
          ' -vf scale=w=720:h=380:force_original_aspect_ratio=decrease ' +
          `${pathes.staticFiles}${pathes.uploadedFiles}${dist}/${filename}` +
          '  2>&1',
      );
      fs.unlinkSync(`${pathes.staticFiles}${pathes.uploadedFiles}${filename}`);
    } else if (mimeType === 2) {
      await shell.exec(
        'sudo  ffmpeg -i   ' +
          `${pathes.staticFiles}${pathes.uploadedFiles}${filename}` +
          '  -vf scale=480:-1  ' +
          `${pathes.staticFiles}${pathes.uploadedFiles}${dist}/${filename}` +
          '  2>&1',
      );
      fs.unlinkSync(`${pathes.staticFiles}${pathes.uploadedFiles}${filename}`);
    } else {
      fs.copyFile(
        `${pathes.staticFiles}${pathes.uploadedFiles}${filename}`,
        `${pathes.staticFiles}${pathes.uploadedFiles}${dist}/${filename}`,
        (err) => {
          if (err) console.log(err);
          fs.unlinkSync(
            `${pathes.staticFiles}${pathes.uploadedFiles}${filename}`,
          );
        },
      );
    }

    const mediaUrl = `${pathes.uploadedFiles}${dist}/${filename}`;
    const file = await this.mediaDataAcceess.createMedia(mediaUrl, mimeType);
    return file;
  }
  async deleteMedia(mediaId) {
    const media = await this.mediaDataAcceess.findById(mediaId);
    if (media && media.ownerId === 0) {
      const fileUrl = `${pathes.staticFiles}/${media.mediaUrl}`;
      try {
        fs.unlinkSync(fileUrl);
        await this.mediaDataAcceess.deleteMedia(mediaId);
        return true;
      } catch (err) {
        console.error(err);
        return false;
      }
    }
    return false;
  }
  async findAllStates(): Promise<StateDto[]> {
    const states = await this.stateDataAccess.findAll();
    return states.map((state) => stateObj(state));
  }
}
