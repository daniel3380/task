import { Controller } from '@nestjs/common';

import { AdminService } from './admin.service';

@Controller('admin')
export class UserController {
  constructor(private readonly adminService: AdminService) {}
}
