import { Module } from '@nestjs/common';

import { UserController } from './admin.controller';
import { AdminService } from './admin.service';

import { Jwt } from '../../common/helpers/jwt.helper';
import { Tools } from '../../common/helpers/tools.helpers';

@Module({
  imports: [],
  controllers: [UserController],
  providers: [AdminService, Jwt, Tools],
})
export class AdminModule {}
