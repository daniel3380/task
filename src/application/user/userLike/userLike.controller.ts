import { Body, Controller, Post, Res } from '@nestjs/common';

import {
  ApiBody,
  ApiHeader,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
} from '@nestjs/swagger';
import { Response } from 'express';

import {
  CreateUserLikeDto,
  ResponseCarLikeDto,
} from '../../../DTO/userLike.dto';
import { UserLikeService } from './userLike.service';

@Controller('UserLike')
@ApiTags('UserLike')
export class UserLikeController {
  constructor(private readonly UserLikeService: UserLikeService) {}
  @ApiOperation({ summary: 'create UserLike' })
  @ApiOkResponse({
    description: 'response create user',
    type: ResponseCarLikeDto,
  })
  @ApiHeader({
    name: 'jtoken',
    description: 'this token received in login',
  })
  @ApiBody({
    description: 'get dto',
    type: CreateUserLikeDto,
  })
  @Post('create')
  async create(
    @Body() createUserLikeDto: CreateUserLikeDto,
    @Res() res: Response,
  ) {
    try {
      const result = await this.UserLikeService.create(
        createUserLikeDto,
        res.locals.user.id,
      );
      return res.status(200).json(result);
    } catch (err) {
      throw err;
    }
  }

  // @ApiOperation({ summary: 'list UserLike' })
  // @ApiHeader({
  //   name: 'jtoken',
  //   description: 'this token received in login',
  // })
  // @ApiOkResponse({
  //   description: 'response updated load',
  //   type: Boolean,
  // })
  // @Get('list')
  // async list(@Query() listCarOptionDto: ListCarOptionDto) {
  //   try {
  //     return await this.UserLikeService.list(listCarOptionDto);
  //   } catch (err) {
  //     throw err;
  //   }
  // }
}
