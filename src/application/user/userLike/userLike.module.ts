import { Module } from '@nestjs/common';
import { UsersDataAcceess } from '../../../dataAccess/users.dataAccess';
import { UserLikeDataAccess } from '../../../dataAccess/userLike.dataAccess';
import { UserLikeController } from './userLike.controller';
import { UserLikeService } from './userLike.service';

@Module({
  imports: [],
  providers: [UserLikeService, UserLikeDataAccess, UsersDataAcceess],
  controllers: [UserLikeController],
})
export class UserLikeModule {}
