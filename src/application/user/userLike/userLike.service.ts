import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { UsersDataAcceess } from '../../../dataAccess/users.dataAccess';
import { UserLikeDataAccess } from '../../../dataAccess/userLike.dataAccess';
import { GlobalService } from '../../../utils/global.service';
import {
  WebSocketGateway,
  SubscribeMessage,
  MessageBody,
} from '@nestjs/websockets';
import { Socket } from 'socket.io';
@Injectable()
export class UserLikeService {
  constructor(
    private readonly userDataAcceess: UsersDataAcceess,
    private readonly userLikeDataAccess: UserLikeDataAccess,
  ) {}
  async create(createUserLikeDto, userSenderId): Promise<any> {
    const { userGetterId, likeType } = createUserLikeDto;
    const user = await this.userDataAcceess.findOneId(userGetterId);
    if (!user) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          error: 'ماشین مورد نظر وجود ندارد',
        },
        HttpStatus.NOT_FOUND,
      );
    }
    const UserLike = await this.userLikeDataAccess.findOneId(
      userGetterId,
      likeType,
      userSenderId,
    );

    if (!UserLike) {
      await this.userLikeDataAccess.create(
        userGetterId,
        likeType,
        userSenderId,
      );
      // const recipientUserId = userGetterId; // شناسه کاربری کاربر دیگر
      // const message = 'Someone liked your post!'; // متن پیام
      // Socket.to(recipientUserId).emit('notification', message);
      // return { userId: userGetterId, message: ' با موفقیت لایک شد ' };
    }
    await this.userLikeDataAccess.destroy(userGetterId, likeType, userSenderId);
    return { userId: userGetterId, message: ' لایک برداشته شد ' };
  }
}
