import { HttpException, HttpStatus, Injectable } from '@nestjs/common';

import axios from 'axios';
import moment from 'moment';
import { userObj } from '../../DTO/user.dto';
import { Jwt } from '../../common/helpers/jwt.helper';
import { Tools } from '../../common/helpers/tools.helpers';
import { SmsRqDataAcceess } from '../../dataAccess/smsRq.dataAccess';
import { UsersDataAcceess } from '../../dataAccess/users.dataAccess';
import { validNumberWithRegex } from '../../utils/validPhoneNumber.regex';
import { faker } from '@faker-js/faker';
import smsRequest = require('request');

const env = process.env.NODE_ENV;

@Injectable()
export class UserService {
  constructor(
    private readonly smsRqDataAcceess: SmsRqDataAcceess,
    private readonly tools: Tools,
    private readonly jwt: Jwt,
    private readonly usersDataAcceess: UsersDataAcceess,
  ) {}

  async login(loginDto, ip) {
    const phone = this.tools.removeZiro(loginDto.phone);
    const validNumber = validNumberWithRegex(phone);
    if (validNumber == false) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_ACCEPTABLE,
          error: 'شماره وارد شده معتبر نیست',
        },
        HttpStatus.NOT_ACCEPTABLE,
      );
    }

    const smsRq = await this.smsRqDataAcceess.findSmsRq(phone, ip);
    if (parseInt(loginDto.type) === 1 || parseInt(loginDto.type) === 2) {
      if (smsRq) {
        if (smsRq.sendCount > 1000) {
          return smsRq.jwtToken;
        }

        // کد من
        const now = moment(new Date());
        const secondsDiff = now.diff(moment(smsRq.lastSentTime), 'seconds');
        if (secondsDiff < 120 && smsRq.mobile === phone) {
          return smsRq.jwtToken;
        }

        let jwtToken = smsRq.jwtToken;
        if (phone !== smsRq.mobile) {
          const tokenValues = {
            phone: phone,
          };
          jwtToken = this.jwt.signer(tokenValues, 2 * 24 * 60 * 60);
        }
        const sendCount = smsRq.sendCount + 1;

        const code = this.tools.codeCreator();

        if (env !== 'development' && env !== 'test') {
          if (parseInt(loginDto.type) === 1) {
            await this.tools.sendSmsCode(loginDto.phone, code);
          } else {
            await this.tools.callCode(loginDto.phone, code);
          }
        }

        await this.smsRqDataAcceess.updateSmsRq(
          smsRq.id,
          jwtToken,
          phone,
          sendCount,
          ip,
          code,
        );

        return jwtToken;
      } else {
        const code = this.tools.codeCreator();

        const tokenValues = {
          phone: phone,
        };
        if (env !== 'development' && env !== 'test') {
          if (parseInt(loginDto.type) === 1) {
            await this.tools.sendSmsCode(loginDto.phone, code);
          } else {
            await this.tools.callCode(loginDto.phone, code);
          }
        }
        const jwtToken = this.jwt.signer(tokenValues);

        await this.smsRqDataAcceess.createSmsRq(jwtToken, phone, ip, code);
        return jwtToken;
      }
    } else {
      throw new HttpException(
        {
          status: HttpStatus.NOT_ACCEPTABLE,
          error: ' نوع ارتباط به درستی وارد نشده است  ',
        },
        HttpStatus.NOT_ACCEPTABLE,
      );
    }
  }

  async faker(): Promise<any> {
    try {
      const minLat = 25.0;
      const maxLat = 39.0;
      const minLon = 44.0;
      const maxLon = 63.0;

      const numberOfUsers = 2;
      for (let i = 0; i < numberOfUsers; i++) {
        const firstName = faker.person.firstName();
        const lastName = faker.person.lastName();
        const username = faker.internet.userName();
        const email = faker.internet.email();
        const avatar = faker.image.avatar();
        const lat = faker.location.latitude({ min: minLat, max: maxLat });
        const lon = faker.location.longitude({ min: minLon, max: maxLon });

        await this.usersDataAcceess.userFaker(
          firstName,
          lastName,
          username,
          email,
          avatar,
          lat,
          lon,
        );
      }
    } catch (err) {
      throw err;
    }
  }

  async verifyCode(tokenVerifyInput): Promise<any> {
    try {
      const { code, phone, token } = tokenVerifyInput;
      const phoneClean = this.tools.removeZiro(phone);
      const tokenValues = this.jwt.verifier(token);
      if (!tokenValues) {
        throw new HttpException(
          {
            status: HttpStatus.UNAUTHORIZED,
            error: ' توکن نا معتبر ',
          },
          HttpStatus.UNAUTHORIZED,
        );
      }

      const rMobile = tokenValues['phone'];
      if (phoneClean !== rMobile) {
        throw new HttpException(
          {
            status: HttpStatus.NOT_ACCEPTABLE,
            error: ' شماره موبایل تغییر کرده است ',
          },
          HttpStatus.NOT_ACCEPTABLE,
        );
      }
      const smsRq = await this.smsRqDataAcceess.validateCode(
        phoneClean,
        parseInt(code),
        token,
      );
      if (!smsRq) {
        throw new HttpException(
          {
            status: HttpStatus.NOT_ACCEPTABLE,
            error: 'کد اشتباه می باشد',
          },
          HttpStatus.NOT_ACCEPTABLE,
        );
      }
      const userExist = await this.usersDataAcceess.findOne(phoneClean);
      if (!userExist) {
        const user = await this.usersDataAcceess.create(phoneClean);
        const tokenValues = {
          userId: user.id,
        };
        const jwtConf = this.jwt.signer(tokenValues, 24 * 60 * 60);
        await this.usersDataAcceess.update(user.id, jwtConf);
        const userSubmit = await this.usersDataAcceess.findOneId(user.id);
        await this.smsRqDataAcceess.deleteSmsRq(smsRq.id);
        await axios
          .post(
            `https://abk-chat-test.iran.liara.run/api/v3/users/signup`,
            {
              firstName: userSubmit.firstName ?? 'null',
              lastName: userSubmit.lastName ?? 'null',
              username: userSubmit.username ?? 'null',
              phone: userSubmit.phone,
            },
            { headers: { 'Content-Type': 'application/json' } },
          )
          .then((res) => {
            console.log('res.data.message');
          })
          .catch((err) => {
            console.log('err');
          });
        return userSubmit;
      } else {
        const tokenValues = {
          userId: userExist.id,
        };
        const jwtConf = this.jwt.signer(tokenValues, 24 * 60 * 60 * 365);
        await this.usersDataAcceess.update(userExist.id, jwtConf);
        const finalUser = await this.usersDataAcceess.findOneId(userExist.id);
        await this.smsRqDataAcceess.deleteSmsRq(smsRq.id);

        await axios
          .post(
            `https://abk-chat-test.iran.liara.run/api/v3/users/signup`,
            {
              firstName: finalUser.firstName ?? 'null',
              lastName: finalUser.lastName ?? 'null',
              username: finalUser.username ?? 'null',
              phone: finalUser.phone,
            },
            { headers: { 'Content-Type': 'application/json' } },
          )
          .then((res) => {
            console.log('res.data.message');
          })
          .catch((err) => {
            console.log('err');
          });
        // const body = ex.body;
        // const myJSON = JSON.parse(body);
        // console.log(myJSON);
        return finalUser;
      }
    } catch (err) {
      throw err;
    }
  }

  async tokenRenew(token) {
    const tokenValues = this.jwt.verifier(token);
    if (!tokenValues) {
      new HttpException(
        {
          status: HttpStatus.UNAUTHORIZED,
          error: ' توکن نامعتبر ',
        },
        HttpStatus.UNAUTHORIZED,
      );
      return;
    }
    // قبلی آیا اکسپایر می شود یا نه
    const user = await this.usersDataAcceess.findUserToken(token);
    const jwtConf = this.jwt.signer({ phone: user.phone }, 24 * 60 * 60);
    user.update({ token: jwtConf });

    const finalUser = await this.usersDataAcceess.findOneId(user.id);
    const url = `https://abk-chat.iran.liara.run/api/v3/users/submit-data`;
    const options = {
      url,
      method: 'POST',
      json: {
        user: finalUser,
      },
      data: finalUser,
    };
    await smsRequest(options);

    return;
  }

  async addNameFamily(NameFamilyDto, userId) {
    const { firstName, lastName } = NameFamilyDto;
    const userIsExist = await this.usersDataAcceess.findOneId(userId);

    if (!userIsExist) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          error: 'کاربری یافت نشد',
        },
        HttpStatus.NOT_FOUND,
      );
    }

    const result = await this.usersDataAcceess.addNameFamily(
      userIsExist.id,
      firstName,
      lastName,
    );

    const userFinal = await this.usersDataAcceess.findOneId(userIsExist.id);

    await this.tools.sendDataChat(
      userFinal.phone,
      userFinal.id,
      userFinal.firstName + ' ' + userFinal.lastName,
    );

    const url = `https://abk-chat.iran.liara.run/api/v3/users/submit-data`;
    const options = {
      url,
      method: 'POST',
      json: {
        user: userFinal,
      },
      data: userFinal,
    };

    await smsRequest(options);

    return { success: true, result };
  }

  async addUsername(username: string, userId) {
    const uniqueUsername = await this.usersDataAcceess.findUsername(username);
    if (uniqueUsername && uniqueUsername.id !== userId) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_ACCEPTABLE,
          error: ' این نام کاربری قبلا انتخاب شده است ',
        },
        HttpStatus.NOT_ACCEPTABLE,
      );
    }
    const result = await this.usersDataAcceess.updateUsername(userId, username);
    const resultUser = await this.usersDataAcceess.findOneId(userId);
    // await axios.post(
    //   'https://abk-chat.iran.liara.run/api/v3/users/submit-data',
    //   {
    //     user: resultUser,
    //   },
    // );
    // const ax = await axios.post(
    //   'https://abk-chat.iran.liara.run/api/v3/users/signup',
    //   {
    //     user: 'resultUser',
    //   },
    // );

    return;
  }

  async checkUsername(username: string, userId) {
    const uniqueUsername = await this.usersDataAcceess.findUsername(username);
    if (uniqueUsername && uniqueUsername.id !== userId) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_ACCEPTABLE,
          error: ' این نام کاربری قبلا انتخاب شده است ',
        },
        HttpStatus.NOT_ACCEPTABLE,
      );
    }

    return { success: true };
  }

  async findOne(userId) {
    const user = await this.usersDataAcceess.findOneId(userId);
    if (!user) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          error: ' کاربری وجود ندارد ',
        },
        HttpStatus.FORBIDDEN,
      );
    }
    return userObj(user);
  }

  async userName(username) {
    const user = await this.usersDataAcceess.findUsername(username);
    if (!user) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          error: ' کاربری وجود ندارد ',
        },
        HttpStatus.FORBIDDEN,
      );
    }
    return userObj(user);
  }
  async sendAvatar(addAvatarDto, userId) {
    const { buffer } = addAvatarDto;
    const user = await this.usersDataAcceess.findOneId(userId);
    if (!user) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          error: ' کاربری وجود ندارد ',
        },
        HttpStatus.FORBIDDEN,
      );
    }
    // const srcImage = await this.cloud.getUploadsList(buffer, 'avatar');
    // const resAvatar = await this.usersDataAcceess.avatar(srcImage, userId);

    // const url = `https://abk-chat.iran.liara.run/api/v3/users/submit-data`;
    // const options = {
    //   url,
    //   method: 'POST',
    //   json: {
    //     user: resAvatar,
    //   },
    //   data: resAvatar,
    // };
    // await smsRequest(options);

    // return resAvatar;
  }

  async aroundMe(userId) {
    const user = await this.usersDataAcceess.findUserAroundMe(userId);
    if (!user) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          error: ' کاربری وجود ندارد ',
        },
        HttpStatus.FORBIDDEN,
      );
    }
    return userObj(user);
  }

  async updateSocket(userId, socketId) {
    return;
  }

  async updateStatus(userId, status) {
    const user = await this.usersDataAcceess.findOne(userId);
    if (!user) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          error: ' کاربری وجود ندارد ',
        },
        HttpStatus.FORBIDDEN,
      );
    }
    await this.usersDataAcceess.update(userId, {
      userStatus: status,
      lastSeen: new Date(),
    });
    return user;
  }
}
