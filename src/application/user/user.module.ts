import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';

import { UserController } from './user.controller';
import { UserService } from './user.service';

import {
  CheckUserMiddleware,
  ValidUserMiddleware,
} from '../../common/middlewares/validateUser.middleware';

import { Jwt } from '../../common/helpers/jwt.helper';
import { Tools } from '../../common/helpers/tools.helpers';
import { SmsRqDataAcceess } from '../../dataAccess/smsRq.dataAccess';
import { UsersDataAcceess } from '../../dataAccess/users.dataAccess';
import { UserLikeModule } from './userLike/userLike.module';

@Module({
  imports: [UserLikeModule],
  controllers: [UserController],
  providers: [UserService, Jwt, Tools, UsersDataAcceess, SmsRqDataAcceess],
})
export class UsersModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(ValidUserMiddleware)
      .exclude('/user/login', '/user/verify')
      .forRoutes('/');

    consumer
      .apply(CheckUserMiddleware)
      .forRoutes('/user/login', '/user/verify');
  }
}
