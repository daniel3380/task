FROM node:18.14.0-alpine 
RUN mkdir /app
WORKDIR /app
COPY package*.json ./
COPY . .
RUN yarn
RUN npm run build
EXPOSE 3000
CMD ["npm","start"]
